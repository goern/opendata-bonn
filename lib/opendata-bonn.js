/*
 *   opendata-bonn
 *   Copyright (C) 2017  Christoph Görn
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

/** @module opendata-bonn */

/** The name of the module. */
const name = 'opendata-bonn';

("use strict");

/**
 * Module dependencies.
 */
const rp = require('request-promise');

const version = '0.1.0';

const AktuelleStraßenverkehrslageEndpoint =
    'http://stadtplan.bonn.de/geojson?Thema=19584&koordsys=25832';

const AlleStrecken = [35, 36, 69, 70];

const KennedyBrückeBeuelNachBonn = 35;
const KennedyBrückeBonnNachBeuel = 36;
const KennedyBrückeBeideRichtungen = [35, 36];

const FriedrichEbertBrückeBeuelNachBonn = 69;
const FriedrichEbertBrückeBonnNachBeuel = 70;
const FriedrichEbertBrückeBeideRichtungen = [69, 70];

var options = {
    headers: {
        "User-Agent": "opendata-bonn/v" + version,
        Accept: "application/json; charset=utf-8" // FIXME this is not handled by server
    },
    resolveWithFullResponse: false, // we want just the result, not the full response headers
    json: true
};

function arraysEqual(a, b) {
    if (a === b) return true;
    if (a == null || b == null) return false;
    if (a.length != b.length) return false;

    // FIXME sort both arrays

    for (var i = 0; i < a.length; ++i) {
        if (a[i] !== b[i]) return false;
    }
    return true;
}

/**
 * Return only the GeoJSON Features of the requested Strecken.
 * 
 * @param {Array<Int>} streckenListe 
 * @return {GeoJSON} geojson.
 */
function GetAktuelleStraßenverkehrslage(streckenListe) {
    options.uri = AktuelleStraßenverkehrslageEndpoint;

    return rp(options)
        .then(function(result) {
            // var reducedResult = Object.assign({}, result);
            var reducedResult = JSON.parse(JSON.stringify(result));

            // walk thru the whole result and...
            reducedResult.features = result.features.filter(function(feature) {
                return streckenListe.includes(feature.properties.strecke_id);
            });

            return reducedResult;
        })
        .catch(function(err) {
            console.log(err);
        });
}

module.exports = {
    // Modulinformationen
    name: name,
    version: version,

    // Webservice Endpunkte
    AktuelleStraßenverkehrslageEndpoint: AktuelleStraßenverkehrslageEndpoint,

    // Streckenabschnitte
    AlleStrecken: AlleStrecken,
    KennedyBrückeBeuelNachBonn: KennedyBrückeBeuelNachBonn,
    KennedyBrückeBonnNachBeuel: KennedyBrückeBonnNachBeuel,
    KennedyBrückeBeideRichtungen: KennedyBrückeBeideRichtungen,
    FriedrichEbertBrückeBeuelNachBonn: FriedrichEbertBrückeBeuelNachBonn,
    FriedrichEbertBrückeBonnNachBeuel: FriedrichEbertBrückeBonnNachBeuel,
    FriedrichEbertBrückeBeideRichtungen: FriedrichEbertBrückeBeideRichtungen,

    // Funktionen
    GetAktuelleStraßenverkehrslage: GetAktuelleStraßenverkehrslage
};