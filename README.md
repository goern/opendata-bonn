# [Offene Daten:Bonn]

Dies ist ein Node.js Framework um (Teile) der unter https://opendata.bonn.de/ bereitgestellten Daten zu nutzen. Es werden nicht alle Datenquellen unterstützt.

## Zusätzliche Informationen

* 'strecke_id' 35 scheint die Kennedy-Brücke in Richtung Bonn zu sein.
* 'strecke_id' 36 scheint die Kennedy-Brücke in Richtung Beuel zu sein.
* 'strecke_id' 69 scheint die Friedrich-Ebert-Brücke in Richtung Bonn zu sein.
* 'strecke_id' 70 scheint die Friedrich-Ebert-Brücke in Richtung Beuel zu sein.

Die Daten werden bereitgestellt von:

    Datenquelle: Bundesstadt Bonn https://www.opendata.bonn.de

Datenbestände weiterer Datenbereitsteller sind wie folgt zu benennen:

    Datenquelle: bonnorange AöR https://opendata.bonn.de
    Datenquelle: BCP GmbH https://opendata.bonn.de
    Datenquelle: Stadtwerke Bonn-https://opendata.bonn.de
    Datenquelle: Cambio https://www.cambio-carsharing.de/
    Datenquelle: Bonn.digital https://bonn.jetzt


# Entwicklung

Dieses Kapitel muss noch geschrieben werden.
[![pipeline status](https://gitlab.com/goern/opendata-bonn/badges/master/pipeline.svg)](https://gitlab.com/goern/opendata-bonn/commits/master)

## Tests

Jede Funktion hat Tests, diese greifen auf die Webservices der Bundesstadt Bonn zu, keiner der Test verwendet 'mocks'.

# Copyright und Lizenz

Der Text der Lizenz ist in der Datei LICENSE mitgeliefert, der deutsche Text ist unter http://www.gnu.de/documents/gpl-3.0.de.html zu finden.

opendata-bonn
Copyright (C) 2017  Christoph Görn <goern@b4mad.net>

Dieses Programm ist freie Software. Sie können es unter den Bedingungen der GNU General Public License, wie von der Free Software Foundation veröffentlicht, weitergeben und/oder modifizieren, entweder gemäß Version 3 der Lizenz oder (nach Ihrer Option) jeder späteren Version.

Die Veröffentlichung dieses Programms erfolgt in der Hoffnung, daß es Ihnen von Nutzen sein wird, aber OHNE IRGENDEINE GARANTIE, sogar ohne die implizite Garantie der MARKTREIFE oder der VERWENDBARKEIT FÜR EINEN BESTIMMTEN ZWECK. Details finden Sie in der GNU General Public License.

Sie sollten ein Exemplar der GNU General Public License zusammen mit diesem Programm erhalten haben. Falls nicht, siehe <http://www.gnu.org/licenses/>.