# Changelog

## [Unveröffentlicht]

`arraysEqual()`

## [0.1.0] - goern - 2017-Nov-12

### Hinzugefügt

`GetAktuelleStraßenverkehrslage()` als Funktion die die aktuelle Straßenverkehrslage von bestimmten Strecken abruft.
### Geändert

`streck_id` was in wrong/reverse order.

### Entfernt

Nothing.