/*
 *   opendata-bonn
 *   Copyright (C) 2017  Christoph Görn
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

'use strict';

var opendatabonn = require('../'); // require it now, so that mockery can do its job
var assert = require('assert');

describe('AktuelleStraßenverkehrslage', function() {

    it('für Stecken 69, 70: soll eine Promise mit GeoJSON (2 features) zurückgeben', function() {
            var lage = opendatabonn.GetAktuelleStraßenverkehrslage(opendatabonn.FriedrichEbertBrückeBeideRichtungen);

            // we got something back...
            assert.notEqual(lage, null);
            return lage.then(function(result) {
                // lets see if its the right thing
                assert.notEqual(result, null);
                assert.notEqual(result.features, null);
                assert.equal(result.features.length, 2);
            });

        }),

        it('für Stecke -1: soll [] zurückgeben werden', function() {
            this.timeout(5000);
            var lage = opendatabonn.GetAktuelleStraßenverkehrslage([-1]);

            // we got something back...
            assert.notEqual(lage, null);
            return lage.then(function(result) {
                // and its [], because strecke_id was not within result
                assert.equal(result.features.length, 0);
            });
        })
});